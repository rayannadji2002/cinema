import { createRouter, createWebHistory } from "vue-router";
import Recherche from "../views/Recherche.vue";
import Accueil from "../views/Accueil.vue";
import Tendance from "../views/Tendance.vue";
import Details from "../views/Details.vue";

const routes = [
  {
    path: "/",
    name: "Accueil",
    component: Accueil,
  },
  {
    path: "/Details",
    name: "details",
    component: Details
  },
  {
    path: "/Recherche",
    name: "recherche",
    component: Recherche,
  },
  {
    path: "/Tendance",
    name: "Tendance",
    component: Tendance,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
